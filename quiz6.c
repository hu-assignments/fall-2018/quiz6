#include <stdio.h>
#include <stdlib.h>

typedef int bool;
#define true 1
#define false 0

#define LIST_LENGTH 10

int *added_list(int *list1, int *list2, int size1, int size2)
{
    int *added = malloc(sizeof(int) * (size1 + size2));
    int i;
    for (i = 0; i < (size1 + size2); i++)
    {
        if (i < size1)
            added[i] = list1[i];
        else
            added[i] = list2[i - size1];
    }
    return added;
}

void print_list(int *list, int size, bool sorted, char list_name)
{
    printf((sorted ? "Sorted List %c: [" : "List %c: ["), list_name);

    int i;
    for (i = 0; i < size; i++)
    {
        if (i == size - 1)
            printf("%d]\n", list[i]);
        else
            printf("%d, ", list[i]);
    }
}

void swap(int *first, int *second)
{
    int temp = *first;
    *first = *second;
    *second = temp;
}

int *sorted(int *list, int size)
{
    int i, j, temp;
    for (i = 0; i < size; ++i)
    {
        for (j = i + 1; j < size; ++j)
        {
            if (list[i] > list[j])
            {
                swap(&(list[i]), &(list[j]));
            }
        }
    }
    return list;
}

int main()
{
    int list_a[LIST_LENGTH] = {12, 29, 15, 8, 36, 6, 9, 2, 4, 7};
    int list_b[LIST_LENGTH] = {39, 41, 1, 3, 27, 14, 5, 11, 90, 43};

    print_list(list_a, LIST_LENGTH, false, 'A');
    print_list(list_b, LIST_LENGTH, false, 'B');
    print_list(sorted(list_a, LIST_LENGTH), LIST_LENGTH, true, 'A');
    print_list(sorted(list_b, LIST_LENGTH), LIST_LENGTH, true, 'B');
    print_list(added_list(list_a, list_b, LIST_LENGTH, LIST_LENGTH), LIST_LENGTH * 2, false, 'C');
    print_list(sorted(added_list(list_a, list_b, LIST_LENGTH, LIST_LENGTH), LIST_LENGTH * 2), LIST_LENGTH * 2, true, 'C');
}